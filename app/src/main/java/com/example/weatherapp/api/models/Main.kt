package com.example.weatherapp.api.models

data class Main(
    var humidity: Int?,
    var temp: Float?
    )

