package com.example.weatherapp.api.models

data class Wind(
    var speed : Double?
)
