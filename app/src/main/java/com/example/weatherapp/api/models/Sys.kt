package com.example.weatherapp.api.models

data class Sys(
    var country: String?,
    val sunrise : Long?,
    val sunset : Long?,
)
