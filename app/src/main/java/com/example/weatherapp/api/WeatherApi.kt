package com.example.weatherapp

import com.example.weatherapp.api.models.WeatherData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("weather?lang=ru&appid=2cb6cb1fd4ebd0d67addeeb82e919110")
    fun getWeatherData(@Query("q") cityName: String) : Call<WeatherData>
}