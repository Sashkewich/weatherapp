package com.example.weatherapp.api.models

data class WeatherData(
    var main: Main?,
    var wind: Wind?,
    var sys: Sys?,
    var name: String?,
    var weather: List<Weather>?,
    var visibility: Int?
)
