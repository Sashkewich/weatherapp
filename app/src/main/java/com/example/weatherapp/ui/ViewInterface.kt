package com.example.weatherapp.ui
import com.example.weatherapp.api.models.WeatherData

interface ViewInterface {
    fun showWeatherData(data: WeatherData)
    fun showError(throwable: Throwable)
    fun showLoading(isLoading:Boolean)
}