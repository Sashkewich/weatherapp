package com.example.weatherapp.ui

interface PresenterInterface {
    fun getWeatherData(city: String)
}