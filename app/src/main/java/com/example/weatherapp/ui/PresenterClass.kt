package com.example.weatherapp.ui

import android.annotation.SuppressLint
import com.example.weatherapp.WeatherApi
import com.example.weatherapp.api.models.WeatherData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class PresenterClass(private val api: WeatherApi) : PresenterInterface {
    private var weatherView: ViewInterface? = null

    override fun getWeatherData(city: String) {
        weatherView?.showLoading(true)

        api.getWeatherData(city).enqueue(
            object : Callback<WeatherData> {
                override fun onResponse(call: Call<WeatherData>, response: Response<WeatherData>) {
                    if (response.isSuccessful && response.body() != null) {
                        val data = response.body()
                        weatherView?.showLoading(false)
                        data?.let {weather ->
                            weatherView?.showWeatherData(weather)
                        }

                    }
                }

                override fun onFailure(call: Call<WeatherData>, t: Throwable) {
                    weatherView?.showLoading(false)
                    t.message?.let { weatherView?.showError(t) }
                }
            }
        )
    }

    fun attach(view: ViewInterface) {
        weatherView = view
    }

    fun detach() {
        weatherView = null
    }
}