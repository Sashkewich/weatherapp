package com.example.weatherapp.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.example.weatherapp.RetrofitClient
import com.example.weatherapp.WeatherApi
import com.example.weatherapp.databinding.ActivityMainBinding
import com.example.weatherapp.api.models.WeatherData
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

private const val CITY_NAME_LENGTH = 1

@SuppressLint("SimpleDateFormat")
class MainActivity : ViewInterface, AppCompatActivity() {
    private val api: WeatherApi = RetrofitClient.getClient().create(WeatherApi::class.java)
    private lateinit var binding: ActivityMainBinding
    private val presenter: PresenterClass = PresenterClass(api)
    private val parseFormat = SimpleDateFormat("HH:mm:ss")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        Timber.plant(Timber.DebugTree())

        presenter.attach(this)

        binding.searchWeatherOfCity.doAfterTextChanged { cityName ->
            if (!cityName.isNullOrEmpty() && cityName.length > CITY_NAME_LENGTH)
                presenter.getWeatherData(cityName.toString())
        }
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    override fun showWeatherData(data: WeatherData) {
        with(binding){
            cityName.text = "${data.name?.replaceFirstChar { it.uppercase()}}, ${data.sys?.country}"
            tempValue.text = "${data.main?.temp?.minus(273.15)?.toInt()}°С"
            weatherSmallText.text = data.weather?.get(0)?.description?.replaceFirstChar{ it.uppercase()}
            windSpeedValue.text = "${data.wind?.speed?.toInt().toString()} м/с"
            humidityValue.text = "${data.main?.humidity?.toString()} %"
            visibilityValue.text = "${data.visibility?.div(1000).toString()} км"
            sunriseValue.text = parseFormat.format(data.sys?.sunrise?.times(1000))
            sunsetValue.text = parseFormat.format(data.sys?.sunset?.times(1000))
        }
    }

    override fun showError(throwable: Throwable) {
        Timber.e("---ERROR -> ${throwable.message}")
    }

    override fun showLoading(isLoading: Boolean) {
        with(binding){
            progress.isVisible = isLoading
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }
}